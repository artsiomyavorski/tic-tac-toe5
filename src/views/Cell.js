import React from 'react';
import PropTypes from 'prop-types';

const Cell = ({onClick, id, size, children}) => {
  return (
    <div
      style={{width: size + '%', height: size + '%'}}
      className="cell"
      onClick={() => onClick(id)}
      id={id}
    >
      {children}
    </div>
  );
};

Cell.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.number,
  size: PropTypes.number.isRequired,
};

export default Cell;
