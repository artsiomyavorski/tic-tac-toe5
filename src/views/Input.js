import React from 'react';
import PropTypes from 'prop-types';

const Input = ({type, onChange, placeholder, min, max, value, label, onSubmit, disabled,}) => {
  return (
    <div className="input-container">
      <div
        className="input-label"
      >
        {label}
      </div>
      <form
        onSubmit={onSubmit}
      >
        <input
          className="input"
          type={type}
          onChange={onChange}
          placeholder={placeholder}
          min={min}
          max={max}
          value={value}
          disabled={disabled}
        />
      </form>
    </div>

  );
};

Input.propTypes = {
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  onSubmit: PropTypes.func,
  disabled: PropTypes.bool
};

export default Input;

