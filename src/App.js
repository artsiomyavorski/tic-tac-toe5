import React, {Component} from 'react';
import './App.css'
import Modal from 'react-modal';

import Panel from './views/Panel'
import Button from './views/Button'
import Input from './views/Input'
import Cell from './views/Cell'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deskSize: '',
      disableInput: false,
      canStart: false,
      values: [],
      crossTime: true,
      modalIsOpen: false,
      winner: ''
    }
  }

  setWinner = (winner) => {
    this.setState({
      winner: winner.toUpperCase(),
    });

    this.openModal()
  }

  openModal = () => {
    this.setState({
      modalIsOpen: true
    });
  }

  closeModal = () => {
    this.setState({
      modalIsOpen: false,
      deskSize: '',
      canStart: false,
      disableInput: false
    });
  }

  handleDeskSize = (e) => {
    this.setState({
      deskSize: e.target.value,
      canStart: false,
    });

  }

  submitDeskSize = (e) => {
    e.preventDefault()

    let size = Number(this.state.deskSize)

    if (this.state.deskSize) {
      this.setState({
        disableInput: true,
        canStart: true,
        values: [...Array(size * size).fill(null)]
      });
    }
  }

  markCell = (id) => {
    let values = this.state.values

    if (!values[id]) {
      this.state.crossTime
        ? values[id] = 'x'
        : values[id] = 'o'

      this.setState({
        values,
        crossTime: !this.state.crossTime
      });

      this.countCoords(id)
    }

  }

  isX = (cell) => {
    return cell === 'x'
  }

  isO = (cell) => {
    return cell === 'o'
  }

  checkWin = ([A0, A1, A2, A3], [B0, B1, B2, B3]) => {

    if (this.isX(A0) && this.state.crossTime) {
      if (this.isX(B0)) {
        if (this.isX(A1)) {
          if (this.isX(B1)) {
            this.setWinner('x')
          }
          if (this.isX(A2)) {
            this.setWinner('x')
          }
        }
        if (this.isX(B1) && this.isX(B2)) {
          this.setWinner('x')
        }
      }
      if (this.isX(A1) && this.isX(A2) && this.isX(A3)) {
        this.setWinner('x')
      }
    }

    if (this.isX(B0) && this.state.crossTime) {
      if (this.isX(A0)) {
        if (this.isX(B1) && this.isX(A1)) {
          this.setWinner('x')
        }
        if (this.isX(A1) && this.isX(A2)) {
          this.setWinner('x')
        }
      }
      if (this.isX(B1) && this.isX(B2) && this.isX(B3)) {
        this.setWinner('x')
      }
    }

    if (this.isO(A0) && !this.state.crossTime) {
      if (this.isO(B0)) {
        if (this.isO(A1)) {
          if (this.isO(B0)) {
            this.setWinner('o')
          }
          if (this.isO(A2)) {
            this.setWinner('o')
          }
        }
        if (this.isO(B0) && this.isO(B2)) {
          this.setWinner('o')
        }
      }
      if (this.isO(A1) && this.isO(A2) && this.isO(A3)) {
        this.setWinner('o')
      }
    }

    if (this.isO(B0) && !this.state.crossTime) {
      if (this.isO(A0)) {
        if (this.isO(B0) && this.isO(A1)) {
          this.setWinner('o')
        }
        if (this.isO(A1) && this.isO(A2)) {
          this.setWinner('o')
        }

      }
      if (this.isO(B0) && this.isO(B2) && this.isO(B3)) {
        this.setWinner('o')
      }
    }
  }

  countCoords = (id) => {
    let cellId = id + 1;
    let size = this.state.deskSize;

    let row = Math.ceil(cellId / size)
    let column = size - (size * row - cellId)


    let cellsToTop = row - 1;
    let cellsToBot = size - row;
    let cellsToLeft = column - 1;
    let cellsToRight = size - column;


    let botArr = [];
    let topArr = [];
    let leftArr = [];
    let rightArr = [];


    if (cellsToBot) {
      for (let i = 0; i < cellsToBot; i++) {
        let bot = this.state.values[id + size * (i + 1)]
        botArr.push(bot)
      }
    }

    if (cellsToTop) {
      for (let i = 0; i < cellsToTop; i++) {
        let top = this.state.values[id - size * (i + 1)]
        topArr.push(top)
      }
    }

    if (cellsToLeft) {
      for (let i = 0; i < cellsToLeft; i++) {
        let left = this.state.values[id - (i + 1)]
        leftArr.push(left)
      }
    }

    if (cellsToRight) {
      for (let i = 0; i < cellsToRight; i++) {
        let right = this.state.values[id + (i + 1)]
        rightArr.push(right)
      }
    }


    this.checkWin(topArr, botArr)
    this.checkWin(leftArr, rightArr)
  }

  start = () => {
    this.setState({
      disableInput: false,
      deskSize: '',
      canStart: false
    });
  }

  render() {
    let size = this.state.deskSize
    let squareSize = size * size
    const {canStart, disableInput, deskSize, crossTime, values, modalIsOpen, winner} = this.state

    return (
      <div className="App">
        <div className="header">
          Крестики-нолики
        </div>

        <Panel>
          {
            canStart && <Button
              onClick={this.start}
            >
              Начать новую игру
            </Button>
          }
          <div className="control">
            <Input
              type="number"
              placeholder="от 5 до 20"
              onChange={this.handleDeskSize}
              onSubmit={this.submitDeskSize}
              value={deskSize}
              min={5}
              max={20}
              label="Размер доски N (N x N)"
              disabled={disableInput}
            />
            {
              !canStart && <div className="starter">
                Нажмите Enter, чтобы построить поле
              </div>
            }
          </div>

        </Panel>
        {
          canStart && <Panel>
            Ход:
            {crossTime
              ? ' X'
              : ' O'}
          </Panel>
        }
        {
          canStart &&
          <div className="desk">
            {
              canStart && [...Array(squareSize)].map((cell, i) => {
                return (
                  <Cell
                    onClick={this.markCell}
                    id={i}
                    key={i}
                    size={100 / size}
                  >
                    { values[i]}
                  </Cell>
                )
              })
            }
          </div>
        }
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
        >
          <div
            className="modal-text"
          >
            Победил {winner}!
          </div>
        </Modal>
      </div>
    );
  }
}

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

export default App;
